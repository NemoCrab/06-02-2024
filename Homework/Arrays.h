#ifndef ARRAYS
#define ARRAYS

int* CreateArray(int);
int* CreateFilledArray(int);
int* CreateRandomArray(int);
int* ArrayResizing(int*, int, int);
int* RemovingLastElementOfArray(int*, int);
int* AddingLastElementOfArray(int*, int);
int* RemovingFirstElementOfArray(int*, int);
int* AddingNElementOfArray(int*, int);
int* RemovingNElementOfArray(int*, int);

int SizeOfIntArray(int*);
int SumValue(int*, int);

double AverageValue(int*, int);

void ArrayFilling(int*, int n);
void CopyArray(int*, int*, int, int);
void CoutArray(int*, int );
void FullFunctionCheck();

#endif
