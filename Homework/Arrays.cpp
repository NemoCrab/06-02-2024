#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Arrays.h"

int* CreateArray(int n) {
	int* arr{ new int[n] }; 
	return arr; }

void ArrayFilling(int* arr, int n) {
	for (int i{ 0 }; i < n; i++)
		arr[i] = 0; }

void RandomArrayFilling(int* arr, int n) {
	for (int i{ 0 }; i < n; i++)
		arr[i] = rand() % 2147483647; }

int* CreateFilledArray(int n) {
	int* arr{ CreateArray(n) };
	ArrayFilling(arr, n);
	return arr; }

int* CreateRandomArray(int n) {
	int* arr{ CreateArray(n) };
	srand(time(0));
	RandomArrayFilling(arr, n);
	return arr;
}

int GetValue() {
	int n;
	std::cin >> n;
	std::cout << "\n";
	return n; }

int BeautifulGetValue() {
	std::cout << "Send new number for array: ";
	return GetValue();
}

void CopyArray(int* oldArray, int* newArray, int n, int firstPosition, int startPosition) {
	for (int i{ firstPosition }; i < n; i++) {
		newArray[i] = oldArray[i - firstPosition];
	}
}

void CopyArray(int* oldArray, int* newArray, int n, int firstPosition, int startPosition, int deletePosition)     {
	for (int i{ firstPosition }; i < n; i++) {
		newArray[i] = oldArray[i-firstPosition+1+deletePosition];
	}
}

int* ArrayResizing(int* oldArray, int n1, int n2) {
	int* newArray{ new int[n2] };

	CopyArray(oldArray, newArray, n1, 0, 0);

	for (int i{ n1 }; i < n2; i++)
	{
		newArray[i] = 0;
	}

	return newArray;
}

void CoutArray(int* array, int SizeOfArray) {
	for (int i{ 0 }; i < SizeOfArray; i++)
	{
		std::cout << array[i] << ' ';
	}
	std::cout << '\n';
}

void BeautifulCoutArray(int* array, int SizeOfArray) {
	std::cout << "Your array: ";

	CoutArray(array, SizeOfArray);

	std::cout << "\n";
}

int* RemovingLastElementOfArray(int* array, int n)
{
	int* newArray{ new int[n-1] };
	CopyArray(array, newArray, n-1, 0, 0);
	return newArray;
}
int* AddingLastElementOfArray(int* array, int n)
{
	int* newArray{ new int[n+1] };
	CopyArray(array, newArray, n, 0, 0);
	newArray[n] = BeautifulGetValue();
	return newArray;
}
int* AddingFirstElementOfArray(int* array, int n)
{
	int* newArray{ new int[n + 1] };
	newArray[0] = BeautifulGetValue();
	CopyArray(array, newArray, n+1, 1, 0);
	return newArray;
}
int* RemovingFirstElementOfArray(int* array, int n)
{
	int* newArray{ new int[n - 1] };
	CopyArray(array, newArray, n - 1, 0, 0, 0);
	return newArray;
}

int* AddingNElementOfArray(int* arr, int arraySize)
{
	int newElement{ BeautifulGetValue() };
	std::cout << "Now, we need position of new element.\n";
	int newPosition{ BeautifulGetValue() };

	int* newArray{ new int[arraySize + 1] };
	
	for (int i = 0; i < newPosition - 1; i++)
	{
		newArray[i] = arr[i];
	}

	newArray[newPosition - 1] = newElement;

	for (int i = newPosition; i < arraySize + 1; i++)
	{
		newArray[i] = arr[i - 1];
	}

	return newArray;
}

int* RemovingNElementOfArray(int* arr, int arraySize)
{
	std::cout << "Which position you want to delete?\n";
	int position{ BeautifulGetValue() };

	int* newArray{ new int[arraySize - 1] };

	for (int i = 0; i < position - 1; i++)
	{
		newArray[i] = arr[i];
	}

	for (int i = position - 1; i < arraySize - 1; i++)
	{
		newArray[i] = arr[i+1];
	}

	return newArray;
}

int SumValue(int* arr, int n)
{
	int summ{ 0 };
	for (int i = 0; i < n; i++)
	{
		summ += arr[i];
	}
	return summ;
}

double AverageValue(int* arr, int n)
{
	return static_cast<double>(SumValue(arr, n)) / n;
}

void MinAndMax(int* arr, int n)
{
	int minn{ arr[0]};
	int maxx{ arr[0]};

	for (int i = 0; i < n; i++)
	{
		if (arr[i] > maxx)
			maxx = arr[i];
		if (arr[i] < minn)
			minn = arr[i];
	}
	std::cout << "The smallest element in array is: " << minn
			  << "\nThe biggest element in array is: " << maxx << '\n';
}

void StartMessage() {
	std::cout << "What task needs to be checked?\n"
		<< "Creating an Array\n"
		<< "|  1)  A function for creating an empty array of a certain size with default values.\n"
		<< "|  2)  Function for creating an array with random values.\n"
		<< "Function for creating an array with random values\n"
		<< "|  3)  Function for changing the size of an array while saving the data.\n"
		<< "Adding and removing elements\n"
		<< "|  4)  A function to add an element to the end of an array.\n"
		<< "|  5)  Function to remove an element from the end of an array.\n"
		<< "|  6)  A function to add an element to the beginning of an array.\n"
		<< "|  7)  Function to remove an element from the beginning of an array.\n"
		<< "|  8)  A function to add an element to a specific array position.\n"
		<< "|  9)  A function to remove an element from a specific array position.\n"
		<< "Aggregation operations\n"
		<< "|  10) Function for calculating the average value of array elements.\n"
		<< "|  11) Function for calculating the sum of array elements.\n"
		<< "|  12) A function to find the minimum and maximum elements in an array.\n"
		<< "\nPass the required number: ";
}

int GetNewSize() {
	std::cout << "Send new size of array: ";
	return GetValue();
}

void SwitchInFunctionCheck(int a) {
		int n{ GetValue() };

		std::cout << "Pass the number of elements in the array: ";

		int arraySize{ GetValue() };

		if (n == 1)//Выполнение первого случая
		{
			int* arr{ CreateFilledArray(arraySize) };
			BeautifulCoutArray(arr, arraySize);
		}
		else if (1 < n < 13)
		{
			int* arr{ CreateRandomArray(arraySize) };//Выполнение второго случая
			BeautifulCoutArray(arr, arraySize);

			switch (n)//Выполнение остальных случаев
			{
			case 3:
			{
				int newSize{ GetNewSize() };
				int* arr2{ ArrayResizing(arr, arraySize, newSize) };
				BeautifulCoutArray(arr2, newSize);
				break;
			}
			case 4:
			{
				int* arr2{ AddingLastElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize + 1);
				break;
				break;
			}
			case 5:
			{
				int* arr2{ RemovingLastElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize - 1);
				break;
			}
			case 6:
			{
				int* arr2{ AddingFirstElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize + 1);
				break;
			}
			case 7:
			{
				int* arr2{ RemovingFirstElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize - 1);
				break;
			}
			case 8:
			{
				int* arr2{ AddingNElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize + 1);
				break;
			}
			case 9:
			{
				int* arr2{ RemovingNElementOfArray(arr, arraySize) };
				BeautifulCoutArray(arr2, arraySize - 1);
				break;
			}
			case 10:
			{
				std::cout << "Average of all elements is: " << AverageValue(arr, arraySize) << '\n';
				break;
			}
			case 11:
			{
				std::cout << "Sum of all elements is: " << SumValue(arr, arraySize) << '\n';
				break;
			}
			case 12:
			{
				MinAndMax(arr, arraySize);
				break;
			}
			default:
				break;
			}
		}
		else
			std::cout << "Wrong!\n";
}

void FullFunctionCheck()
{
	int a{ 1 };
	StartMessage();
	SwitchInFunctionCheck(a);
}
