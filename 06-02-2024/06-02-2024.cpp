﻿#include <iostream>

using namespace std;

int Sum(int, int);
void Swap(int* a, int* b);
void Swap(int& a, int& b);

short a = 1, b = 2, c = 3, d = 4, e = 5, f = 6, g = 7;

int main()
{
	int k, m, l, n;
	cout << "Global variables:" << endl;
	cout << " " << &a << " " << &b << endl;
	cout << "Local variables:" << endl;
	cout << " " << &k << " " << &m << " " << &l << " " << &n << endl;
	cout << "Function main:" << endl;
	cout << "&main " << &main << endl;
	cout << "Function Sum:" << &Sum << endl;

	cout << " " << a << " " << b << " " << c << " " << d << " " << e << " " << f << " " << g << endl;
	cout << " " << &a << " " << &b << " " << &c << " " << &d << " " << &e << " " << &f << " " << &g << endl;
	short* p = &d, * q = &d;
	cout << " p -> " << *p << endl;
	cout << " q -> " << *q << endl;
	p++;
	cout << " p++ -> " << *p << endl;
	p--;
	cout << " p-- -> " << *p << endl;
	q--;
	cout << " q-- -> " << *q << endl;
	q++;
	cout << " q++ -> " << *q << endl;
	p += 4;
	cout << " p+=4 -> " << *p << endl;
	q -= 60;
	cout << " q-=3 -> " << *q << endl;
	cout << (p == q) << " " << (p <= q) << " " << (p < q) << " " << (p > q) << " " << (p >= q) << " " << (p != q) << endl;

	short array[5] = { 0 };
	cout << "Name of array A is pointer = " << array << " " << &array << " " << &array[0] << endl;
	for (int i = 0; i < 6; i++)
		cout << " &array[" << i << "] = " << &array[i] << endl;

	system("pause");
	return 0;
}

int Sum(int, int)
{
	return a + b;
}

void Swap(int* a, int* b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

void Swap(int& a, int& b)
{
	int t = a;
	a = b;
	b = t;
}